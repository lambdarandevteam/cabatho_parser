<?php
$servername = "localhost";
$username = "root";
$password = "admin";
$dbname = "new_eclated";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if(isset($_POST['submit']) && isset($_FILES)) {
$FileType = strtolower(pathinfo($_FILES["xlsx_file"]["name"],PATHINFO_EXTENSION));
$FileType = strtolower(pathinfo($_FILES["xlsx_file"]["name"],PATHINFO_EXTENSION));
$target_file = "xlsx_uploaded/".$_FILES["xlsx_file"]["name"];
if($FileType == "xlsx") {
    if (move_uploaded_file($_FILES["xlsx_file"]["tmp_name"], $target_file)) {
        save_file($_FILES["xlsx_file"]["name"],$conn);
    } else {
        header('HTTP/1.0 403 Forbidden');
        echo "Sorry, there was an error uploading your file.";
    }
}else{
    echo "Veuillez choisir un fichier de type xlsx";
}
}else{
}
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<div class="container mt-4">
    <div class="row">
    <form method="POST" action="" enctype="multipart/form-data">
    <input type="file" name="xlsx_file" />
    <input type="submit" name="submit" value="Valider" class="btn btn-info" />
</form>
    </div>
</div>
    <?php
    function save_file($file_name,$conn){
    $rownb = 0;
    $allpath = scandir('datas');
    require_once('vendor/SimpleXLSX.php');
    if ( $xlsx = SimpleXLSX::parse("xlsx_uploaded/".$file_name) ) {
    $allparsed = $xlsx->rows();
    foreach ($allparsed as $path){
        if($rownb > 0){
            $to_save = [
                $path[0], //nom_produit
                $path[1], //categorie
                $path[2], //quantite
                $path[3],  //image
                $path[4], //marque
                $path[5], //modele
                $path[6], //type_produit
                $path[7], //description
                $path[8], //vue_eclate
                $path[9], //sku
            ];
            $verify_sql = "SELECT nom_produit FROM produit WHERE nom_produit = '$path[0]' ";
            $result_verif = $conn->query($verify_sql);
            if(count($result_verif->fetch_assoc())==0){

            $sql = "INSERT INTO produit (nom_produit, categorie, quantite, image, marque, modele, type_produit, description, vue_eclate,sku) VALUES ('$path[0]', '$path[1]', '$path[2]', '$path[3]', '$path[4]', '$path[5]', '$path[6]', '$path[7]', '$path[8]','$path[9]')";

            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            }else{
                // echo "donée déja inseré";
            }
         }
        $rownb++;
        }
    } else {
    echo SimpleXLSX::parseError();
}
}
    $list_query = "SELECT * FROM produit";
    $preslist = $conn->query($list_query);
 ?>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Num</th>
            <th scope="col">Nom produit</th>
            <th scope="col">images produit</th>
            <th scope="col">image vue eclatee</th>
        </tr>
    </thead>
    <tbody>
    <?php while ($items = $preslist->fetch_assoc()) { ?>
    <tr>
        <td><?php echo $items['id'] ;?></td>
        <td><?php echo $items['nom_produit'] ;?></td>
        <td><?php echo $items['image'] ;?>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal<?php echo $items['id']; ?>">
               voir image
        </button>
        </td>
        <td><?php echo $items['vue_eclate'] ;?>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalEclated<?php echo $items['id']; ?>">
               voir image
        </button>
        </td>
    </tr>
        <div class="modal" id="myModal<?php echo $items['id']; ?>">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Images <?php echo $items['nom_produit']; ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php 
                        if(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."1.jpg")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."1.jpg";
                        }elseif(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."1.png")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."1.png";
                        }elseif(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."1.gif")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."1.gif";
                        }

                        if(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."2.jpg")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."2.jpg";
                        }elseif(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."2.png")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."2.png";
                        }elseif(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."2.gif")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."2.gif";
                        }

                        if(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."3.jpg")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."3.jpg";
                        }elseif(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."3.png")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."3.png";
                        }elseif(is_file("datas/".$items['nom_produit']."/images/".$items['nom_produit']."3.gif")){
                            $path1 = "datas/".$items['nom_produit']."/images/".$items['nom_produit']."3.gif";
                        }

                        if(is_file("datas/".$items['nom_produit']."/images/vue_eclatee/".$items['nom_produit']."_vue_eclatee.jpg")){
                            $patheclated = "datas/".$items['nom_produit']."/images/vue_eclatee/".$items['nom_produit']."_vue_eclatee.jpg";
                        }elseif(is_file("datas/".$items['nom_produit']."/images/vue_eclatee/".$items['nom_produit']."_vue_eclatee.png")){
                            $patheclated = "datas/".$items['nom_produit']."/images/vue_eclatee/".$items['nom_produit']."_vue_eclatee.png";
                        }elseif(is_file("datas/".$items['nom_produit']."/images/vue_eclatee/".$items['nom_produit']."_vue_eclatee.gif")){
                            $patheclated = "datas/".$items['nom_produit']."/images/vue_eclatee/".$items['nom_produit']."_vue_eclatee.gif";
                        }
                        ?>
                        <img class="img-fluid w-100" src="<?php echo $path1; ?>">
                        <img class="img-fluid w-100" src="<?php echo $path2; ?>">
                        <img class="img-fluid w-100" src="<?php echo $path3; ?>">
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="myModalEclated<?php echo $items['id']; ?>">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Vue éclatée <?php echo $items['nom_produit']; ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <img class="img-fluid w-100" src="<?php echo $patheclated; ?>">
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    </tbody>
</table>
